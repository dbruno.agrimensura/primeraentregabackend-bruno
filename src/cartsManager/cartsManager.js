const fs = require('fs').promises;

class CartManager {
    constructor(filePath) {
        this.filePath = filePath;
    }

    async createCart(newCart) {
        try {
            // Leer los datos de los carritos desde el archivo
            const carts = await this.getCarts();
            // Generar un nuevo ID para el carrito
            const newCartId = carts.length > 0 ? Math.max(...carts.map(cart => cart.id)) + 1 : 1;
            // Agregar el nuevo carrito al arreglo
            newCart.id = newCartId;
            newCart.products = [];
            carts.push(newCart);
            // Guardar los datos actualizados en el archivo
            await fs.writeFile(this.filePath, JSON.stringify(carts, null, 2));
            // return newCartId;
            return `se ha creado el carrito con el id: ${newCartId}`
        } catch (error) {
            throw new Error(`Error al crear el carrito: ${error.message}`);
        }
    }

    async getCartById(cartId) {
        try {
            // Leer los datos de los carritos desde el archivo
            const carts = await this.getCarts();
            // Buscar el carrito por su ID
            const cart = carts.find(cart => cart.id === cartId);
            return cart || null;
        } catch (error) {
            throw new Error(`Error al obtener el carrito: ${error.message}`);
        }
    }

    async addProductToCart(cartId, productId, quantity) {
        try {
            // Leer los datos de los carritos desde el archivo
            const carts = await this.getCarts();
            // Buscar el carrito por su ID
            const cartIndex = carts.findIndex(cart => cart.id === cartId);
            if (cartIndex !== -1) {
                // Agregar el producto al carrito
                const product = { productId, quantity };
                carts[cartIndex].products.push(product);
                // Guardar los datos actualizados en el archivo
                await fs.writeFile(this.filePath, JSON.stringify(carts, null, 2));
                // return true;
                return "se ha añadido correctamente el producto"
            } else {
                throw new Error('Carrito no encontrado');
            }
        } catch (error) {
            throw new Error(`Error al agregar producto al carrito: ${error.message}`);
        }
    }

    async getCarts() {
        try {
            // Leer los datos de los carritos desde el archivo
            const data = await fs.readFile(this.filePath, 'utf8');
            return JSON.parse(data);
        } catch (error) {
            if (error.code === 'ENOENT') {
                // El archivo no existe, retorna un arreglo vacío
                return [];
            } else {
                throw new Error(`Error al obtener los carritos: ${error.message}`);
            }
        }
    }
}

module.exports = CartManager;


