const express = require('express');

const path = require('path');

const ProductManager = require('../productManager/productManager');


// Rutas para productos
const productsRouter = express.Router();
const productsFilePath = path.join(__dirname, '../data/ProductList.json');

productsRouter.use(express.json());

// Crear una instancia de ProductManager
const productManager = new ProductManager(productsFilePath) 

// Middleware para obtener la lista de productos con opcionalidad de límite
productsRouter.get('/',async (req, res) => {
    console.log('se ejecuto  productsRouter get');
    // Leer el parámetro de consulta 'limit'
    const limit = parseInt(req.query.limit);

    let products = await productManager.getProducts();
console.log(products)
        res.json(products);
    });

// Middleware para obtener un producto por su ID
productsRouter.get('/:pid',async (req, res) => {
    const productId = req.params.pid;

    // Obtener el producto por su ID desde ProductManager
    const Id = parseInt(productId);
    const product = await productManager.getProductById(Id);

    if (product) {
        res.json(product);
    } else {
        res.status(404).send('Producto no encontrado');
    }
});

// Middleware para agregar un nuevo producto
productsRouter.post('/',async (req, res) => {
    const newProduct = req.body;
    // Lógica para agregar un nuevo producto
    const result = await productManager.addProduct(newProduct)
    res.json({ message: result })

});

// Middleware para actualizar un producto por su ID
productsRouter.put('/:pid',async (req, res) => {
    const productId = req.params.pid;
    const updatedProduct = req.body;
    // Lógica para actualizar un producto por su ID
    if (productId && !isNaN(productId)) {
        const idNumber = parseInt(productId);
        const result = await productManager.updateProduct(idNumber, updatedProduct)

        res.json({ message: result })

    } else {
        res.send('Error: Se requiere el id del producto')
    }
 
});

// Middleware para eliminar un producto por su ID
productsRouter.delete('/:pid',async (req, res) => {
    const productId = req.params.pid;
    // Lógica para eliminar un producto por su ID
    if (productId && !isNaN(productId)) {
        const idNumber = parseInt(productId);
        const result = await productManager.deleteProduct(idNumber)

        res.json({ message: result })

    } else {
        res.send('Error: Se requiere el id del producto')
    }

});

module.exports = productsRouter;
