const path = require('path');
const express = require('express');
const CartManager = require('../../src/cartsManager/cartsManager');

const cartsRouter = express.Router();
const cartsFilePath = path.join(__dirname, '../data/Carritos.json');
const cartManager = new CartManager(cartsFilePath);

cartsRouter.post('/', async (req, res) => {
    try {
        const newCartId = await cartManager.createCart(req.body);


        res.status(201).json(newCartId);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

cartsRouter.get('/:cid', async (req, res) => {
    const cartId = parseInt(req.params.cid);
    try {
        const cart = await cartManager.getCartById(cartId);
        if (cart) {
            res.json(cart);
        } else {
            res.status(404).json({ error: 'Carrito no encontrado' });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

cartsRouter.post('/:cid/product/:pid', async (req, res) => {
    const cartId = parseInt(req.params.cid);
    const productId = parseInt(req.params.pid);
    const { quantity } = req.body;
    try {
        const success = await cartManager.addProductToCart(cartId, productId, quantity);
        if (success) {
            res.send('Producto agregado al carrito exitosamente');
        } else {
            res.status(404).json({ error: 'Carrito no encontrado' });
        }
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

module.exports = cartsRouter;