const express = require('express');

const app = express();
const port = 8080;

// rutas
const productsRouter = require('./routes/productRoute.js');
const cartsRouter = require('./routes/cartsRoute.js')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/api/products', productsRouter);
app.use('/api/carts', cartsRouter);

app.listen(port, () => {
    console.log(`Servidor Express corriendo en http://localhost:${port}`);
});